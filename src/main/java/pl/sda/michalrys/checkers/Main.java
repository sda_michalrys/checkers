package pl.sda.michalrys.checkers;

import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static final char CHECK_WHITE = 'w';
    public static final char CHECK_WHITE_QUEEN = 'W';
    public static final char CHECK_BLACK = 'b';
    public static final char CHECK_BLACK_QUEEN = 'B';
    public static final char CHECK_MAX_AMOUNT = 12;
    public static final char CHECK_ROWS_AMOUNT = 3;
    public static final char EMPTY_FIELD = ' ';
    public static final char PROHIBITED_FIELD = '\'';
    public static final int BOARD_LIMIT_MIN_ROW = 1;
    public static final int BOARD_LIMIT_MAX_ROW = 8;
    public static final int BOARD_LIMIT_MIN_COLUMN = 1;
    public static final int BOARD_LIMIT_MAX_COLUMN = 8;


    public static final String MESSAGE_START = "Checkers.";
    public static final String MESSAGE_CURRENT_STATUS = "Current status of the game board:";
    public static final String MESSAGE_GET_LOCATION = "Give location of check: ";

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        // run program
        System.out.println(MESSAGE_START);

        // setup
        int gameLoop = 0;
        char[][] checkersBoard = initializeGameBoard();
        checkersBoard = startingPositionGameBoard(checkersBoard);
        int[] location;

        while (true) {
            gameLoop++; // TODO na tym skonczylem
            printBoard(checkersBoard, MESSAGE_CURRENT_STATUS);

            location = getLocation(scanner, MESSAGE_GET_LOCATION, checkersBoard);
            System.out.println("location is: " + Arrays.toString(location));

            break;
        }

    }

    public static char[][] initializeGameBoard() {
        char[][] checkersBoard = new char[BOARD_LIMIT_MAX_ROW][BOARD_LIMIT_MAX_COLUMN];
        char[] fields = {EMPTY_FIELD, PROHIBITED_FIELD};

        for (int i = 0; i < BOARD_LIMIT_MAX_ROW; i++) {
            for (int j = 0; j < BOARD_LIMIT_MAX_COLUMN; j++) {
                checkersBoard[i][j] = fields[(j + i) % 2];
            }
        }

        return checkersBoard;
    }

    public static char[][] startingPositionGameBoard(char[][] checkersBoard) {
        char[][] checkersBoardStartPosition = checkersBoard;
        // put white checks
        for (int i = 0; i < CHECK_ROWS_AMOUNT; i++) {
            for (int j = 0; j < BOARD_LIMIT_MAX_COLUMN; j++) {
                if (checkersBoardStartPosition[i][j] == EMPTY_FIELD) {
                    checkersBoardStartPosition[i][j] = CHECK_WHITE;
                }
            }
        }
        // put black checks
        for (int i = BOARD_LIMIT_MAX_ROW - CHECK_ROWS_AMOUNT; i < BOARD_LIMIT_MAX_ROW; i++) {
            for (int j = 0; j < BOARD_LIMIT_MAX_COLUMN; j++) {
                if (checkersBoardStartPosition[i][j] == EMPTY_FIELD) {
                    checkersBoardStartPosition[i][j] = CHECK_BLACK;
                }
            }
        }

        return checkersBoardStartPosition;
    }

    public static void printBoard(char[][] checkersBoard, String messageInfo) {
        int markMaxLength = 1;

        // print out
        System.out.println();
        System.out.println(messageInfo);

        // pring upper boundary
        for (int j = 0; j < checkersBoard[0].length; j++) {
            if (j == 0) {
                System.out.print("  -");
                System.out.print(j + 1);
                System.out.print("- ");
            }
            if (j > 0 && j < checkersBoard[0].length - 1) {
                System.out.print("-");
                System.out.print(j + 1);
                System.out.print("- ");
            }
            if (j == checkersBoard[0].length - 1) {
                System.out.print("-");
                System.out.print(j + 1);
                System.out.print("-");
            }
        }
        System.out.println();

        for (int i = 0; i < checkersBoard.length; i++) {
            System.out.print((i + 1) + "|");

            for (int j = 0; j < checkersBoard[0].length; j++) {

                System.out.print(" " + checkersBoard[i][j] + " ");

                if (j < checkersBoard[0].length - 1) {
                    System.out.print("|");
                }
                if (j == checkersBoard[0].length - 1) {
                    System.out.print("|" + (i + 1));
                    System.out.println();
                }
            }
            // print out horizontal separator
            if (i < checkersBoard.length - 1) {
                // - margin left
                System.out.print("  -");
                for (int k = 0; k < checkersBoard[0].length; k++) {
                    for (int j = 1; j <= markMaxLength; j++) {
                        // - below mark symbol
                        System.out.print("-");
                    }
                    if (k != checkersBoard[0].length - 1) {
                        // - column separator
                        System.out.print("-+-");
                    }
                }
                // - margin right
                System.out.println("-");
            }
        }
        // pring lower boundary
        for (int j = 0; j < checkersBoard[0].length; j++) {
            if (j == 0) {
                System.out.print("  -");
                System.out.print(j + 1);
                System.out.print("- ");
            }
            if (j > 0 && j < checkersBoard[0].length - 1) {
                System.out.print("-");
                System.out.print(j + 1);
                System.out.print("- ");
            }
            if (j == checkersBoard[0].length - 1) {
                System.out.print("-");
                System.out.print(j + 1);
                System.out.print("-");
            }
        }
        System.out.println("\n");
    }

    public static int [] getLocation(Scanner scanner, String message, char[][] checkersBoard) {
        int[] location = new int[2];
        String userInput;

        System.out.println(message);
        userInput = scanner.nextLine();

        try {
            location[0] = Integer.parseInt(userInput.split(",")[0]);
            location[1] = Integer.parseInt(userInput.split(",")[1]);

        } catch (NumberFormatException exc) {
            // coordinate is not integer
            return null;
        } catch (ArrayIndexOutOfBoundsException exc) {
            //
            return null;
        }

        // validation
        if (location[0] < BOARD_LIMIT_MIN_ROW || location[0] > BOARD_LIMIT_MAX_ROW) {
            return null;
        }
        if (location[1] < BOARD_LIMIT_MIN_COLUMN || location[1] > BOARD_LIMIT_MAX_COLUMN) {
            return null;
        }
        return location;
    }

    public static int[] chooseCheck(char[][] checkersBoard, int[] location) {
        return null;
    }

}
